/**Exeptions handling class*/
package ru.sortItApp.exeptions;
import ru.sortItApp.enums.ErrorCode;


public class ErrorCodeExeption {
        public static String processErrorCodes(ReadWriteExeption e) throws ReadWriteExeption {
            ErrorCode errorCode = ErrorCode.valueOf(e.getErrorCode().toUpperCase());
            System.out.println(errorCode.getDESCRIPTION());
            throw e;
        }
}