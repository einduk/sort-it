/**Exeptions handling class
 Extending class Exception*/
package ru.sortItApp.exeptions;


public class ReadWriteExeption extends Exception{
    private final String errorCode;
    public ReadWriteExeption(String message, String errorCode) throws ReadWriteExeption {
        super(message);
        this.errorCode = errorCode;
    }
    public String getErrorCode(){
        return this.errorCode;
    }

}