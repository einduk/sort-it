/** Define a custom method for outputting the log.  Extending class Formatter */
package ru.sortItApp.logging;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Date;
import java.util.logging.Formatter;
import java.util.logging.LogRecord;


public class FormaterLogger extends Formatter {
    @Override
    public String format(LogRecord record) {
        StringBuilder stringFormat = new StringBuilder();
        stringFormat.append(new Date(record.getMillis()))
                .append(" {")
                .append(record.getSourceClassName().replaceAll("^.*?(\\w+)\\W*$", "$1"))
                .append("} -> ")
                .append(record.getLevel().getLocalizedName())
                .append(": ")
                .append(formatMessage(record))
                .append(System.getProperty("line.separator"));

        if (record.getThrown() != null) {
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            record.getThrown().printStackTrace(pw);
            pw.close();
            stringFormat.append(sw);
        }
        return stringFormat.toString();
    }
}