/**Step-by-step parameter processing class*/
package ru.sortItApp.configuration;

import ru.sortItApp.enums.ConsoleMessages;
import ru.sortItApp.exeptions.ErrorCodeExeption;
import ru.sortItApp.exeptions.ReadWriteExeption;
import ru.sortItApp.enums.ErrorCode;
import ru.sortItApp.processing.PreProcessing;
import ru.sortItApp.processing.SortController;
import ru.sortItApp.writer.WritingFile;

import java.util.ArrayList;
import java.util.Scanner;
import java.util.stream.Collectors;

import static java.util.stream.Stream.of;
import static ru.sortItApp.Sort_It.logger;


public class ReadingConsoleParameters {
    private static String repeat = "N";
    private static final Scanner IN = new Scanner(System.in);

    static void consoleReading(ArrayList<String> filesInputList) throws ReadWriteExeption {
        while (repeat.equals("N")) {
            do {
                logger.info(ConsoleMessages.CLOSE.getDescription());
                repeat = IN.nextLine().toUpperCase();
            } while (!repeat.equals("N") && !repeat.equals("Y"));
            if (repeat.equals("Y"))
                break;
            logger.info(ConsoleMessages.MANUAL.getDescription());
            do {
                logger.info(ConsoleMessages.SORTING_MODE.getDescription());
                SortController.setSortMode(IN.nextLine());
            } while (!SortController.getSortMode().equals("-a") && !SortController.getSortMode().equals("-d"));

            do {
                logger.info(ConsoleMessages.DATA_TYPE.getDescription());
                PreProcessing.setTypeData(IN.nextLine());
            } while (!PreProcessing.getTypeData().equals("-s") && !PreProcessing.getTypeData().equals("-i"));

            do {
                logger.info(ConsoleMessages.OUTPUT_FILE.getDescription());
                WritingFile.setResultFIleName(IN.nextLine());
            } while (WritingFile.getResultFIleName().isEmpty());

            String fileNames;
            do {
                logger.info(ConsoleMessages.INPUT_FILES.getDescription());
                fileNames = IN.nextLine();
                if (!filesInputList.isEmpty())
                    filesInputList.clear();
                filesInputList.addAll(of(fileNames.split(" "))
                        .collect(Collectors.toList()));
            } while (fileNames.isEmpty());
            try {
                PreProcessing.argumentsParserProcessing(filesInputList);
            } catch (ReadWriteExeption e) {
                logger.severe(ErrorCodeExeption.processErrorCodes(e));
                throw new ReadWriteExeption(e.getMessage(), ErrorCode.FILE_NOT_FOUND_EXCEPTION.getERROR_NAME());
            }
            logger.info(ConsoleMessages.COMPLETED.getDescription());
        }
        IN.close();
    }
}