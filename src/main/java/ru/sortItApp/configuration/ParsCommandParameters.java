/** Console command processing class*/
package ru.sortItApp.configuration;

import ru.sortItApp.enums.ConsoleMessages;
import ru.sortItApp.exeptions.ReadWriteExeption;
import ru.sortItApp.processing.PreProcessing;
import ru.sortItApp.processing.SortController;
import ru.sortItApp.writer.WritingFile;

import java.util.*;

import static ru.sortItApp.Sort_It.logger;


public class ParsCommandParameters {
    private static final ArrayList<String> FILESINPUTLIST = new ArrayList<>();

    public ParsCommandParameters(String[] args) throws ReadWriteExeption {
        ArrayList<String> params = new ArrayList<>(List.of(args));

        if (args.length > 0) {
            if ((args[0].equals("-s") || args[0].equals("-i")) && args.length > 3) {
                SortController.setSortMode("-a");
                PreProcessing.setTypeData(args[0]);
                WritingFile.setResultFIleName(args[1]);
                FILESINPUTLIST.addAll(params.subList(2, params.size()));
                PreProcessing.argumentsParserProcessing(FILESINPUTLIST);
                logger.info(ConsoleMessages.COMPLETED.getDescription());
            } else if ((args[0].equals("-a") || args[0].equals("-d")) && args.length > 4) {
                SortController.setSortMode(args[0]);
                PreProcessing.setTypeData(args[1]);
                WritingFile.setResultFIleName(args[2]);
                FILESINPUTLIST.addAll(params.subList(3, params.size()));
                PreProcessing.argumentsParserProcessing(FILESINPUTLIST);
                logger.info(ConsoleMessages.COMPLETED.getDescription());
            } else {
                logger.warning(ConsoleMessages.INCORRECTLYPARAM.getDescription());
            }
        } else {
            logger.warning(ConsoleMessages.EMPTYPARAM.getDescription());
        }
        ReadingConsoleParameters.consoleReading(FILESINPUTLIST);
    }
}