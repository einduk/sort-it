/**User interaction*/
package ru.sortItApp.enums;


public enum ConsoleMessages {
    SORTING_MODE("Select the sorting mode (ascending -a, descending -d): "),
    DATA_TYPE("Select the data type (-s or -i): "),
    OUTPUT_FILE("Enter the name of the output file: "),
    INPUT_FILES("Enter the names of the processed files separated by a space: "),
    MANUAL("Enter the program parameters following the procedure described below:\n" +
            "1. sorting mode (-a or -d);\n" +
            "2. data type (-s or -i), required;\n" +
            "3. the name of the output file, required;\n" +
            "4. the other parameters are the names of the input files, at least one. (separated by a space)\n"),
    COMPLETED("The program has completed!\n"
            + " The processed file is located in the 'target/classes/outputFile' directory."),
    CLOSE("End program execution (Y or N?): "),
    INCORRECTLYPARAM("You entered the parameters incorrectly!"),
    EMPTYPARAM("You have not filled in the file processing parameters, please enter the parameters!");
    private final String DESCRIPTION;

    public String getDescription() {
        return DESCRIPTION;
    }

    ConsoleMessages(String DESCRIPTION) {
        this.DESCRIPTION = DESCRIPTION;
    }
}