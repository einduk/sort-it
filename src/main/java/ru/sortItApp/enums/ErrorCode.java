/**Enum with description of error codes*/
package ru.sortItApp.enums;


public enum ErrorCode {

    BAD_FILE_TYPE("BAD_FILE_TYPE", "Bad File Type, notify user"),
    FILE_NOT_FOUND_EXCEPTION("FILE_NOT_FOUND_EXCEPTION", "Check the presence of the 'inputFiles' and 'outputFile' " +
            "directories in the /target/classes directory and the files stored in them."),
    FILE_CLOSE_EXCEPTION("FILE_CLOSE_EXCEPTION", "File Close failed."),
    READ_FILE_EXCEPTION("READ_FILE_EXCEPTION", "The content data type does not match the type specified in the parameters. " +
            "Change the source data"),
    NUMBER_OF_FILES_EXCEPTION("NUMBER_OF_FILES_EXCEPTION", "The number of files specified in the parameter " +
            "does not correspond to the number of files in the inputFiles directory");
    private final String ERROR_NAME;
    private final String DESCRIPTION;

    public String getDESCRIPTION() {
        return DESCRIPTION;
    }

    public String getERROR_NAME() {
        return ERROR_NAME;
    }

    ErrorCode(String ERROR_NAME, String DESCRIPTION) {
        this.ERROR_NAME = ERROR_NAME;
        this.DESCRIPTION = DESCRIPTION;
    }
}