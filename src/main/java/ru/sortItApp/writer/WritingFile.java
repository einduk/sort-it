package ru.sortItApp.writer;

import ru.sortItApp.exeptions.ErrorCodeExeption;
import ru.sortItApp.exeptions.ReadWriteExeption;
import ru.sortItApp.enums.ErrorCode;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import static ru.sortItApp.Sort_It.logger;

public class WritingFile {
    private static String resultFIleName;

    @SuppressWarnings("rawtypes")
    public static void processWritingFile(Comparable[] resultArray) {
        try {
            File dirOutPutFile = new File("bin/classes/outputFile");
            File outPutFile = new File("bin/classes/outputFile/" + resultFIleName);
            if (!dirOutPutFile.exists()) {
                dirOutPutFile.mkdirs();
            } else if (!outPutFile.exists()) {
                try {
                    outPutFile.createNewFile();
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }

            try (BufferedWriter writeFile = new BufferedWriter(new FileWriter(outPutFile))) {
                for (Comparable i : resultArray) {
                    writeFile.write(String.valueOf(i));
                    writeFile.newLine();
                }
            } catch (IOException e) {
                throw new ReadWriteExeption(e.getMessage(), ErrorCode.FILE_NOT_FOUND_EXCEPTION.getERROR_NAME());
            }
        } catch (ReadWriteExeption e) {
            try {
                logger.severe(ErrorCodeExeption.processErrorCodes(e));
            } catch (ReadWriteExeption ex) {
                throw new RuntimeException(ex);
            }
        }
    }
    public static String getResultFIleName () {
        return resultFIleName;
    }
    public static void setResultFIleName (String resultFIleName){
        WritingFile.resultFIleName = resultFIleName;
    }
}