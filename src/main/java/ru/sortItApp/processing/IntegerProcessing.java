/** Integer data processing class. Calling the sorting algorithm */
package ru.sortItApp.processing;

import ru.sortItApp.exeptions.ReadWriteExeption;
import ru.sortItApp.enums.ErrorCode;
import ru.sortItApp.writer.WritingFile;

import java.util.concurrent.ForkJoinPool;
import java.util.ArrayList;

import static ru.sortItApp.readers.ReadLinesInToList.allLinesFile;


public class IntegerProcessing {
    @SuppressWarnings("unchecked")
    static void numericArrayProcessing(ForkJoinPool pool, int processorCount) throws ReadWriteExeption {
        ArrayList<Integer> mergeListInt = new ArrayList<>(allLinesFile.size());
        for (String myInt : allLinesFile) {
            try {
                mergeListInt.add(Integer.valueOf(myInt));
            } catch (NumberFormatException e) {
                throw new ReadWriteExeption(e.getMessage(), ErrorCode.READ_FILE_EXCEPTION.getERROR_NAME());
            }
        }

        Integer[] array = mergeListInt.toArray(new Integer[0]);
        Comparable<Integer>[] sortedArray = new Integer[array.length];
        int arrayPartsSize = array.length / processorCount;
        if (arrayPartsSize < 8150) {
            sortedArray = SortController.sort(array);
            WritingFile.processWritingFile(sortedArray);
        } else {
            ArrayList<MultiSortController> sortedPartArray = new ArrayList<>();
            int k = 0;
            for (int i = 0; i < processorCount; i++) {
                Integer[] part = new Integer[arrayPartsSize];
                if (i < processorCount - 1) {

                    System.arraycopy(array, i * arrayPartsSize, part, 0, arrayPartsSize);
                    k++;
                } else {
                    part = new Integer[array.length - arrayPartsSize * k];
                    System.arraycopy(array, i * arrayPartsSize, part, 0, array.length - arrayPartsSize * k);
                }

                MultiSortController multiSortController = new MultiSortController(part);
                pool.invoke(multiSortController);
                sortedPartArray.add(multiSortController);
            }
            int j = 0;
            for (MultiSortController multiSortController : sortedPartArray) {
                if (j == 0) {
                    sortedArray = multiSortController.getSorted();
                    j += 1;
                } else {
                    Comparable<Integer>[] part = multiSortController.getSorted();
                    sortedArray = SortController.compareAndMerge(sortedArray, part);
                }
            }
            WritingFile.processWritingFile(sortedArray);
        }
    }
}