/**Preprocessing class.Data type definition and
 * calling the appropriate processing method*/
package ru.sortItApp.processing;

import ru.sortItApp.exeptions.ReadWriteExeption;
import ru.sortItApp.readers.ReadLinesInToList;

import java.util.ArrayList;
import java.util.concurrent.ForkJoinPool;


public class PreProcessing {
    private static String typeData;

    public static void argumentsParserProcessing(ArrayList<String> filesInputList) throws ReadWriteExeption {
        ReadLinesInToList.readFilesToList(filesInputList);
        int processorCount = Runtime.getRuntime().availableProcessors();
        ForkJoinPool pool = new ForkJoinPool(processorCount - 1);
        if (typeData.equals("-i")) {
            IntegerProcessing.numericArrayProcessing(pool, processorCount);
        } else {
            StringProcessing.stingArrayProcessing(pool, processorCount);
        }
    }
    public static String getTypeData() {
        return typeData;
    }

    public static void setTypeData(String typeData) {
        PreProcessing.typeData = typeData;
    }
}