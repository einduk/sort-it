package ru.sortItApp.processing;

import ru.sortItApp.exeptions.ReadWriteExeption;
import java.util.concurrent.RecursiveAction;


class MultiSortController extends RecursiveAction {
    @SuppressWarnings("rawtypes")
    private final Comparable[] unsortedArray;
    @SuppressWarnings("rawtypes")
    private volatile Comparable[] sortedArray;
    @SuppressWarnings("rawtypes")
    public MultiSortController(Comparable[] unsortedArray) {
        this.unsortedArray = unsortedArray;
    }
    @SuppressWarnings("rawtypes")
    public Comparable[] getSorted() {
        return sortedArray;
    }

    @Override
    protected void compute() {
        try {
            sortedArray = SortController.sort(unsortedArray);
        } catch (ReadWriteExeption e) {
            throw new RuntimeException(e);
        }
    }
}