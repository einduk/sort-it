/** String data processing class. Calling the sorting algorithm */
package ru.sortItApp.processing;

import ru.sortItApp.exeptions.ErrorCodeExeption;
import ru.sortItApp.exeptions.ReadWriteExeption;
import ru.sortItApp.enums.ErrorCode;
import ru.sortItApp.writer.WritingFile;

import java.util.ArrayList;
import java.util.concurrent.ForkJoinPool;

import static ru.sortItApp.Sort_It.logger;
import static ru.sortItApp.readers.ReadLinesInToList.allLinesFile;

class StringProcessing {
    @SuppressWarnings("unchecked")
    static void stingArrayProcessing(ForkJoinPool pool, int processorCount) throws ReadWriteExeption {
        for (String s : allLinesFile) {
            if (s.contains(" ")) {
                throw new ReadWriteExeption("There is a separator in the file ' '. Change the source data",
                        ErrorCode.READ_FILE_EXCEPTION.getERROR_NAME());
            }
        }
        String[] array = allLinesFile.toArray(new String[0]);
        Comparable<String>[] sortedArray = new String[array.length];
        int arrayPartsSize = array.length / processorCount;
        if (arrayPartsSize < 8150) {
            sortedArray = SortController.sort(array);
        } else {
            ArrayList<MultiSortController> sortedPartArray = new ArrayList<>();
            int k = 0;
            for (int i = 0; i < processorCount; i++) {
                String[] part = new String[arrayPartsSize];
                if (i < processorCount - 1) {

                    System.arraycopy(array, i * arrayPartsSize, part, 0, arrayPartsSize);
                    k++;
                } else {
                    part = new String[array.length - arrayPartsSize * k];
                    System.arraycopy(array, i * arrayPartsSize, part, 0, array.length - arrayPartsSize * k);
                }

                MultiSortController multiSortController = new MultiSortController(part);
                pool.invoke(multiSortController);
                sortedPartArray.add(multiSortController);
            }
            int j = 0;
            for (MultiSortController multiSortController : sortedPartArray) {
                if (j == 0) {
                    sortedArray = multiSortController.getSorted();
                    j += 1;
                } else {
                    Comparable<String>[] part = multiSortController.getSorted();
                    sortedArray = SortController.compareAndMerge(sortedArray, part);
                }
            }
        }
        WritingFile.processWritingFile(sortedArray);
    }
}