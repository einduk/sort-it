package ru.sortItApp.processing;

import ru.sortItApp.exeptions.ReadWriteExeption;

public class SortController {
    private static String sortMode;
    @SuppressWarnings("rawtypes")
    static Comparable[] sort(Comparable[] unsortedArray) throws ReadWriteExeption {
        Comparable[] result;
        if (unsortedArray == null || unsortedArray.length == 0) {
            throw new ReadWriteExeption("", "Проверьте исходные данные");
        }
        if (unsortedArray.length <= 1) {
            result = unsortedArray;
        } else {
            Comparable[] leftPart = new Comparable[unsortedArray.length / 2];
            System.arraycopy(unsortedArray, 0, leftPart, 0, unsortedArray.length / 2);
            Comparable[] rightPart = new Comparable[unsortedArray.length - unsortedArray.length / 2];
            System.arraycopy(unsortedArray, unsortedArray.length / 2, rightPart, 0, unsortedArray.length - unsortedArray.length / 2);
            leftPart = sort(leftPart);
            rightPart = sort(rightPart);
            result = compareAndMerge(leftPart, rightPart);
        }

        return result;
    }
    @SuppressWarnings("rawtypes")
    static Comparable[] compareAndMerge(Comparable[] leftPart, Comparable[] rightPart) {

        Comparable[] sortedArray = new Comparable[leftPart.length + rightPart.length];
        int indexLeft = 0, indexRight = 0;

        for (int i = 0; i < sortedArray.length; i++) {
            if (indexLeft == leftPart.length) {
                sortedArray[i] = rightPart[indexRight];
                indexRight++;
            } else if (indexRight == rightPart.length) {
                sortedArray[i] = leftPart[indexLeft];
                indexLeft++;
            } else if (compar(leftPart[indexLeft], rightPart[indexRight])) {
               if(sortMode.equals("-d")) {
                   sortedArray[i] = rightPart[indexRight];
                   indexRight++;
               }else{
                   sortedArray[i] = leftPart[indexLeft];
                   indexLeft++;
               }
            } else {
                if(sortMode.equals("-d")){
                    sortedArray[i] = leftPart[indexLeft];
                    indexLeft++;
                }else {
                    sortedArray[i] = rightPart[indexRight];
                    indexRight++;
                }
            }
        }
        return sortedArray;
    }
    @SuppressWarnings({"rawtypes", "unchecked"})
    private static boolean compar(Comparable leftArrayElement, Comparable rightArrayElement){

        return leftArrayElement.compareTo(rightArrayElement) < 0;
    }
    public static void setSortMode(String sortMode) {

        SortController.sortMode = sortMode;
    }
    public static String getSortMode() {
        return sortMode;
    }
}

