package ru.sortItApp;

import ru.sortItApp.configuration.ParsCommandParameters;
import ru.sortItApp.exeptions.ReadWriteExeption;
import ru.sortItApp.logging.FormaterLogger;

import java.io.IOException;
import java.util.logging.*;

public class Sort_It {
    public static Logger logger = Logger.getLogger(String.class.getName());

    public static void main(String[] args) throws ReadWriteExeption {
        Handler consoleHandler = new ConsoleHandler();
        consoleHandler.setFormatter(new FormaterLogger());
        try {
            LogManager.getLogManager().readConfiguration();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        logger.setUseParentHandlers(false);
        logger.addHandler(consoleHandler);
        new ParsCommandParameters(args);
    }
}