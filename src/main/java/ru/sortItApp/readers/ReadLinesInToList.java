package ru.sortItApp.readers;

import ru.sortItApp.exeptions.ErrorCodeExeption;
import ru.sortItApp.exeptions.ReadWriteExeption;
import ru.sortItApp.enums.ErrorCode;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;


public class ReadLinesInToList {
    public static ArrayList<String> allLinesFile = new ArrayList<>();

    public static void readFilesToList(ArrayList<String> filesInputList)  {

        if (!allLinesFile.isEmpty())
            allLinesFile.clear();

        for (String name : filesInputList) {
            try {
                BufferedReader readFile = new BufferedReader(new FileReader("src/main/resources/inputFiles/" + name));
                while (readFile.ready()) {
                    allLinesFile.add(readFile.readLine());
                }
                readFile.close();
            } catch (IOException e) {
                try {
                    throw new ReadWriteExeption(e.getMessage(), ErrorCode.FILE_NOT_FOUND_EXCEPTION.getERROR_NAME());
                } catch (ReadWriteExeption ex) {
                    throw new RuntimeException(ex);
                }
            }
        }
    }
}